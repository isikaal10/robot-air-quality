const express = require('express');
const app = express();
const request = require('request');
const mongoose = require("mongoose");

// Modeles de donnees mongoose
const PollutionCity = require('./models/m-PollutionCity');
const PollutionRegion = require('./models/m-PollutionRegion');

const CityForecast = require('./models/m-CityForecast');



const MONGO_USER = process.env.MONGO_USER;
const MONGO_PWD = process.env.MONGO_PWD;
const MONGO_CLUSTER = process.env.MONGO_CLUSTER;

const INSERT_EACH_5_HOURS = 18000000;
const INSERT_EACH_5_SECONDS = 5000;
const UPDATE_EACH_3_MINUTES = 180000

//mongoose.connect(`mongodb+srv://ylerroux:Panam75015@cluster0.hp7ow.mongodb.net/air_quality_db_regions?retryWrites=true&w=majority`,
mongoose.connect('mongodb://localhost:27017/air_quality_db',
//mongoose.connect('mongodb://localhost:27017/test',

{ useNewUrlParser: true,
    useUnifiedTopology: true })
	.then(() => console.log('Connexion à MongoDB réussie !'))
	.catch(() => console.log('Connexion à MongoDB échouée !'));

	setInterval(getPollutionByRegionToday, INSERT_EACH_5_SECONDS);
	
	// ******** init des variables communes DEBUT ***********
	region_Api="";
	regionNb_Api=0;

	const cities = ['paris','orleans','tours','besancon','dijon', 'lille','amiens',
	'le havre','rouen','caen', 'ajaccio', 'bastia', 'strasbourg',
	'troyes', 'nantes', 'saint-nazaire', 'la-roche-sur-yon',
	'rennes', 'brest','quimper', 'bordeaux', 'limoges', 'toulouse', 
	'montpellier', 'lyon', 'saint-etienne', 'grenoble',
	'marseille', 'nice'
];
// ******** init des variables communes FIN ***********

// traitement pour alimenter donnees regions du jour
var tabRegionToSave = [];
function  getPollutionByRegionToday(){
	const citiesForRegion = cities;

				for (const element of citiesForRegion) {
					const city = JSON.stringify(element);
				url = "http://api.waqi.info/feed/"+element+"/?token=dc1344cd36c9cafe553b59a89e93636b4ee004eb" ;//ici avec api-key
				request(url, function(error, res, body) {
					if(res.statusCode==200){
						let respValue = JSON.parse(body);
			
						var city_region_Api = element;
						var aqi_ville_region_Api;
						var dayTime_ville_region_Api;
						var cityLatitude_ville_region_Api;
						var cityLongitude_ville_region_Api
						var city_ville_region_Api = element;
						
			
						feedRegions(city_region_Api);
			
						if(respValue.data.aqi == null ||  respValue.data.aqi === "-"){
							aqi_ville_region_Api = 0;
						}
						else{
							aqi_ville_region_Api = respValue.data.aqi;
						}
						if(respValue.data.city.geo[0] != null && respValue.data.city.geo[1] != null ){
							cityLatitude_ville_region_Api = respValue.data.city.geo[0];
							cityLongitude_ville_region_Api = respValue.data.city.geo[1];
					   }
					   if(respValue.data.time["s"] != null){
						dayTime_ville_region_Api = respValue.data.time["s"];
						  }
			
						var pollutionRegion = new PollutionRegion({
							regionName: region_Api,
							regionNumber: regionNb_Api,
							cityName: city_ville_region_Api,
							day: dayTime_ville_region_Api,
							cityGeoLatitude: cityLatitude_ville_region_Api,
							cityGeoLongitude: cityLongitude_ville_region_Api,
							pollutionIndexLastUpdate: aqi_ville_region_Api
				
						});
						
						tabRegionToSave.push(pollutionRegion);
				}
			});
				}	
	  for(let e of tabRegionToSave){
		  PollutionRegion.findOneAndUpdate({cityName: e.cityName},
			 {$set:
				{
				cityName: e.cityName,
				day:e.day, 
				regionName: e.regionName,
				regionNumber: e.regionNumber,
				pollutionIndexLastUpdate: e.pollutionIndexLastUpdate,
				//cityGeoLatitude: e.cityGeoLatitude,
				//cityGeoLongitude: e.cityGeoLongitude
			} }
				 ,{new: true, upsert:true}, (err, doc) => {
					if (err) {
						console.log("Something wrong when updating data!");
					}	  
					console.log("doc "+ doc);
				});
	  }

}


function feedRegions(cityParam){
	switch (cityParam) {
		case 'paris':
			region_Api="Ile-de-France"
			regionNb_Api = 11;
			break;
		case 'lille':
		case 'amiens':
			region_Api = "Hauts-de-France";
			regionNb_Api = 32;
			break;
		case 'le havre':
		case 'rouen':
		case 'caen':
			region_Api ="Normandie";
			regionNb_Api = 28;
			break;
		case 'rennes':
		case 'brest':	
		case 'quimper':
			region_Api ="Bretagne";
			regionNb_Api = 53;
			break;
		case 'nantes':
		case 'saint-nazaire':
		case 'la-roche-sur-yon':
			region_Api="Pays-de-la-Loire";
			regionNb_Api = 52;
			break;
		case 'orleans':
		case 'tours':
			region_Api ="Centre-val-de-Loire";
			regionNb_Api = 24;
			break;
		case 'bordeaux':
		case 'limoges':
			region_Api ="Nouvelle Aquitaine";
			regionNb_Api = 75;
			break;
		case 'montpellier':
		case 'toulouse':
			region_Api ="Occitanie";
			regionNb_Api = 76;
			break;
		case 'marseille':
		case 'nice':
			region_Api="Paca";
			regionNb_Api = 93;
			break;
		case 'lyon':
		case 'saint-etienne':
		case 'grenoble':
			region_Api ="Auvergne-Rhone-Alpes";
			regionNb_Api = 84;
			break;
		case 'dijon':
		case 'besancon':
			region_Api ="Bourgogne-Franche-Comte";
			regionNb_Api = 27;
			break;
		case 'troyes':
		case 'strasbourg':
			region_Api ="Grand Est";
			regionNb_Api = 44;
			break;
		case 'ajaccio':
		case 'bastia':
			region_Api ="Corse";
			regionNb_Api = 94;
			break;
		default:
			console.log('');
	}
}

	app.listen(8282 , function () {
		console.log("http://localhost:8282");
	  });