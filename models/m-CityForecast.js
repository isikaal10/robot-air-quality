const mongoose = require ('mongoose');

const CityForecastSchema = mongoose.Schema ({
    
    cityName: {type: String, require: true},
    pollutionIndex: {type: Number},
    day:   {type: Date},
    cityGeoLatitude: {type: Number},
    cityGeoLongitude: {type: Number},
    regionNumber:{type:Number, require:true},
    regionName:{type:String},
    uvi:{type:Number}
   /* o3:{type:Number},
    t:{type:Number},
    h:{type:Number},
    w:{type:Number},
    p:{type:Number}*/

});

//no2 dioxyde d' azote /  o3 ozone / t temperature / h humidity / w vent  / p pression atmospherique / uvi ultra violets
 

module.exports = mongoose.model('CityForecast', CityForecastSchema);



