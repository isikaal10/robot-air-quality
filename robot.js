const express = require('express');
const app = express();
const request = require('request');
const mongoose = require("mongoose");

// Modeles de donnees mongoose
const PollutionCity = require('./models/m-PollutionCity');
const PollutionRegion = require('./models/m-PollutionRegion');

const CityForecast = require('./models/m-CityForecast');



const MONGO_USER = process.env.MONGO_USER;
const MONGO_PWD = process.env.MONGO_PWD;
const MONGO_CLUSTER = process.env.MONGO_CLUSTER;

const INSERT_EACH_5_HOURS = 18000000;
const INSERT_EACH_5_SECONDS = 5000;

//mongoose.connect(`mongodb+srv://ylerroux:Panam75015@cluster0.hp7ow.mongodb.net/air_quality_db_regions?retryWrites=true&w=majority`,
mongoose.connect('mongodb://localhost:27017/air_quality_db',
//mongoose.connect('mongodb://localhost:27017/test',

{ useNewUrlParser: true,
    useUnifiedTopology: true })
	.then(() => console.log('Connexion à MongoDB réussie !'))
	.catch(() => console.log('Connexion à MongoDB échouée !'));


	setInterval(getPollutionDataByCity, 5000);
// ******** init des variables communes DEBUT ***********
	region_Api="";
	regionNb_Api=0;

	const cities = ['paris','orleans','tours','besancon','dijon', 'lille','amiens',
	'le havre','rouen','caen', 'ajaccio', 'bastia', 'strasbourg',
	'troyes', 'nantes', 'saint-nazaire', 'la-roche-sur-yon',
	'rennes', 'brest','quimper', 'bordeaux', 'limoges', 'toulouse', 
	'montpellier', 'lyon', 'saint-etienne', 'grenoble',
	'marseille', 'nice'
];
// ******** init des variables communes FIN ***********


// recuperation depuis l'api des donnees de pollution pour la ville de paris
function getPollutionDataByCity(){
	const citiesForPollutioncities = cities;
	for (const element of citiesForPollutioncities) {
		const city = JSON.stringify(element);
 url = "http://api.waqi.info/feed/"+element+"/?token=dc1344cd36c9cafe553b59a89e93636b4ee004eb" ;//ici avec api-key
 request(url, function(error, response, body) {
	if(response.statusCode==200){

		let respValue = JSON.parse(body);
		var city_Api = element;
		var  pollutionIndex_Api="";
		var dayTime_Api=""
		var cityLatitude_Api="";
		var cityLongitude_Api="";
		var no2_Api;
		var o3_Api;
		var so2_Api
		var co_Api;
		var t_Api;
		var h_Api;
		var w_Api;
		var p_Api;
		var aqi_Api;
		var pollutionIndexForecastAvg_Api;
		var dayForecast_Api;
		var tabDailyForecastPm25_Api = [];

		feedRegions(city_Api);

		//console.log("ville Q air => " + JSON.stringify(city));

		if(respValue.data.iaqi.hasOwnProperty('pm25')){
			pollutionIndex_Api = respValue.data.iaqi.pm25["v"] ;
			//console.log("index Q air pm25 => " + JSON.stringify(respValue.data.iaqi.pm25["v"] ));
		}
		else if(respValue.data.iaqi.hasOwnProperty('pm10')){
			pollutionIndex_Api = respValue.data.iaqi.pm10["v"] ;
			//console.log("index Q air => pm10" + JSON.stringify(respValue.data.iaqi.pm10["v"] ));
		}
		
		if(respValue.data.time["s"] != null){
			 dayTime_Api = respValue.data.time["s"];
			 console.log("dayTime_Api "+ dayTime_Api); 
		}
		if(respValue.data.city.geo[0] != null && respValue.data.city.geo[1] != null ){
			 cityLatitude_Api = respValue.data.city.geo[0];
			 cityLongitude_Api = respValue.data.city.geo[1];
		}
		if(respValue.data.iaqi.hasOwnProperty('co')){
			co_Api = respValue.data.iaqi.co["v"];
	   }
		if(respValue.data.iaqi.hasOwnProperty('no2')){
			no2_Api = respValue.data.iaqi.no2["v"];
	   }
	   if(respValue.data.iaqi.hasOwnProperty('o3')){
			o3_Api = respValue.data.iaqi.o3["v"];
   }
   	  if(respValue.data.iaqi.hasOwnProperty('so2')){
			so2_Api = respValue.data.iaqi.so2["v"];
}
   	   if(respValue.data.iaqi.hasOwnProperty('t')){
			t_Api = respValue.data.iaqi.t["v"];
}
	  if(respValue.data.iaqi.hasOwnProperty('h')){
			h_Api = respValue.data.iaqi.h["v"];
}
	  if(respValue.data.iaqi.hasOwnProperty('w')){
			w_Api = respValue.data.iaqi.w["v"];
}
	  if(respValue.data.iaqi.hasOwnProperty('p')){
	p_Api = respValue.data.iaqi.p["v"];
}
if(respValue.data.aqi != null){
	aqi_Api = respValue.data.aqi;
	//console.log("aqi "+aqi_Api);
}
/*if(respValue.data.forecast.daily.hasOwnProperty('pm25')){	
	var pm25  = respValue.data.forecast.daily.pm25;
}
else if(respValue.data.forecast.daily.hasOwnProperty('pm10')){
	pollutionIndexForecastAvg_Api= respValue.data.forecast.daily.pm10["avg"];
	 dayForecast_Api = respValue.data.forecast.daily.pm10["day"];
	 var pm10  = respValue.data.forecast.daily.pm10;
	
}*/
		// Model mongoose de type PollutionCity
		var pollutionCity = new PollutionCity({
			cityName : city_Api, 
			pollutionIndex : pollutionIndex_Api,
			day: dayTime_Api,
			cityGeoLatitude: cityLatitude_Api,
			cityGeoLongitude: cityLongitude_Api,
			regionName: region_Api,
			regionNumber: regionNb_Api,
			no2:no2_Api,
			o3:o3_Api,
			so2: so2_Api,
			co: co_Api,
			t: t_Api,
			h: h_Api,
			w:w_Api ,
			p: p_Api,
			pollutionIndexLastUpdate: aqi_Api

		});
		pollutionCity.save();		
		console.log("city_api "+ city_Api + "-- region "+ region_Api + "-- regionNB " + regionNb_Api);

			}
		});
	}	
}

function feedRegions(cityParam){
	switch (cityParam) {
		case 'paris':
			region_Api=" Ile-de-France"
			regionNb_Api = 11; 
			break;
		case 'lille':
		case 'amiens':
			region_Api = "Hauts-de-France";
			regionNb_Api = 32;
			break;
		case 'le havre':
		case 'rouen':
		case 'caen':
			region_Api ="Normandie";
			regionNb_Api = 28;
			break;
		case 'rennes':
		case 'brest':	
		case 'quimper':
			region_Api ="Bretagne";
			regionNb_Api = 53;
			break;
		case 'nantes':
		case 'saint-nazaire':
		case 'la-roche-sur-yon':
			region_Api="Pays-de-la-Loire";
			regionNb_Api = 52;
			break;
		case 'orleans':
		case 'tours':
			region_Api ="Centre-val-de-Loire";
			regionNb_Api = 24;
			break;
		case 'bordeaux':
		case 'limoges':
			region_Api ="Nouvelle Aquitaine";
			regionNb_Api = 75;
			break;
		case 'montpellier':
		case 'toulouse':
			region_Api ="Occitanie";
			regionNb_Api = 76;
			break;
		case 'marseille':
		case 'nice':
			region_Api="Paca";
			regionNb_Api = 93;
			break;
		case 'lyon':
		case 'saint-etienne':
		case 'grenoble':
			region_Api ="Auvergne-Rhone-Alpes";
			regionNb_Api = 84;
			break;
		case 'dijon':
		case 'besancon':
			region_Api ="Bourgogne-Franche-Comte";
			regionNb_Api = 27;
			break;
		case 'troyes':
		case 'strasbourg':
			region_Api ="Grand Est";
			regionNb_Api = 44;
			break;
		case 'ajaccio':
		case 'bastia':
			region_Api ="Corse";
			regionNb_Api = 94;
			break;
		default:
			console.log('');
	}
}

	app.listen(8282 , function () {
		console.log("http://localhost:8282");
	  });